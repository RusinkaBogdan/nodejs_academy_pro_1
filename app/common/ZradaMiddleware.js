var zrada = (req, res, next) => {
	if (req.body.name.toLowerCase().indexOf('зрада') !== -1){
		var io = req.app.get('socketio');
		io.emit('zrada');
		res.status(400).send('zrada');
	} else {
		next();
	}
}

module.exports = zrada;