const express = require('express');
const task = express.Router();

const taskService = require('./taskService');
const zrada = require('../../common/ZradaMiddleware');

task.get('/', (req, res, next) => {
	taskService.getAllTasks().then((tasks) => {
		res.send(tasks);
	}).catch((err) => {
		res.status(400).end();
	});
});

task.post('/', zrada, (req, res, next) => {
	taskService.addTask(req.body).then((task) => {
		var io = req.app.get('socketio');
		io.emit('task-added', task);
		res.status(201).send(task);
	}).catch((err) => {
		console.log(err.message);
		res.status(400).end();
	});
});

task.get('/:id', (req, res, next) => {
	taskService.getTaskById(req.params.id).then((task) => {
		res.send(task);
	}).catch((err) => {
		res.status(400).end();
	});
});

task.put('/:id', zrada, (req, res, next) => {
	taskService.editTask(req.params.id, req.body).then((task) => {
		var io = req.app.get('socketio');
		io.emit('task-edited', {value: req.body.name, _id: req.params.id});
		res.end();
	}).catch((err) => {
		console.log(err)
		res.status(400).end();
	});
});

task.delete('/:id', (req, res, next) => {
	taskService.deleteTask(req.params.id).then(() => {
		var io = req.app.get('socketio');
		io.emit('task-deleted', {_id: req.params.id});
		res.status(200).end();
	}).catch((err) => {
		res.status(400).end();
	});
});

module.exports = task;
