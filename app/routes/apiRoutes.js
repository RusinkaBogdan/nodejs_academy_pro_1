const tasks = require('../entities/tasks/taskAPIRoutes');

const initializeRoutes = (app, io) => {
	app.use('/api/task', tasks);
}

module.exports = initializeRoutes;