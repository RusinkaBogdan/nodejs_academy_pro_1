const tasks = require('../entities/tasks/taskViewRoutes');

const initializeRoutes = (app) => {
	app.use('/', tasks);
	app.use('/user', tasks);
}

module.exports = initializeRoutes;